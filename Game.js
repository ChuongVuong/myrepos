const GameState = Object.freeze({
    SPAWN: Symbol("spawn"),
    PLANE: Symbol("plane"),
    LAND: Symbol("land"),
    SWORD: Symbol("sword"),
    GUN: Symbol("gun"),
    BUILDING: Symbol("building"),
    DIE: Symbol("die")
});

export default class Game{
    constructor(){
        this.stateCur = GameState.SPAWN;
    }
    
    makeAMove(sInput)
    {
        let sReply = "";
        switch(this.stateCur){
            case GameState.SPAWN:
                sReply = "You are on a plane, Boom !!!, the plane has been shot with fire, the plane is falling, Do you take the parachute and jump or Do you wanna stay ?";
                this.stateCur = GameState.PLANE;
                break;
            case GameState.PLANE:
                if(sInput.toLowerCase().match("stay")){
                    sReply = "The plane is still falling, and crash, you die";
                }else{
                    sReply ="You jump with the parachute and land on the ground, there is a gun and a sword on the ground, which one you pick up ?";
                    this.stateCur = GameState.LAND;
                }
                break;
            case GameState.LAND:
                if(sInput.toLowerCase().match("sword")){
                    sReply = "A zombie appear, hit it with the sword ?"
                    this.stateCur = GameState.SWORD;
                }else if(sInput.toLowerCase().match("gun")){
                    sReply = "A zombie appear, shoot it ?";
                    this.stateCur = GameState.GUN;
                }
                break;

            case GameState.SWORD:
                if(sInput.toLowerCase().match("hit")){
                    sReply = "The zombie is dead, there is a building, will you enter ?"
                    this.stateCur = GameState.BUILDING;
                }else {
                    sReply = "The Zombie is coming to you, hit it with the sword?";
                    this.stateCur = GameState.SWORD;
                }
                break;

            case GameState.GUN:
            if(sInput.toLowerCase().match("shoot")){
                sReply = "The zombie is dead, but the gun sound is too loud, more zombie are coming, there is a building, will you enter ?"
                this.stateCur = GameState.BUILDING;
            }else {
                sReply = "The Zombie is coming to you, shoot it ?";
                this.stateCur = GameState.GUN;
            }
            break;

            case GameState.BUILDING:
                if(sInput.toLowerCase().match("enter")){
                    sReply = "There are a group of survivors in the building, you are safe now";
                    this.stateCur = GameState.SAFE;

                }else{
                    sReply = "You die";
                    this.stateCur = GameState.DIE;
    
                }
                break;
        }
        return([sReply]);
    }
}